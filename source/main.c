#include <stdio.h>
#include "dice.h"

int main(){
	int n;
	//Inicializando uma seed para que o numero de rollDice() seja aleatorio
	initializeSeed();
	printf("Quantas faces tem o dado? ");
	scanf("%d", &n);
	//Imprimindo o resultado do rolamento do dado
	printf("Let`s roll the dice: %d\n", rollDice(n));
	return 0;
}
